.. manganeet documentation master file, created by
   sphinx-quickstart on Mon Feb 17 03:15:57 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Manganeet
=====================================
Read online all complete serial manga or manhwa for free.

I know there is a lot similar site like this, but many of them is not so easy to find the complete serial manga or manhwa. So I created this website to collect them to make easier for you who likes to read serial manga or manhwa.

Visit: `https://manganeet.com <https://manganeet.com>`_

Please follow our social network

Twitter: `https://twitter.com/manganeet <https://twitter.com/manganeet>`_

.. toctree::
   :maxdepth: 2
   :caption: Contents:
